package com.study.r2dbcexample1.model.id

import com.fasterxml.jackson.annotation.JsonValue
import java.util.*

data class PersonId(@get:JsonValue val value: UUID = UUID.randomUUID())