package com.study.r2dbcexample1.model

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.data.annotation.Id
import org.springframework.data.domain.Persistable
import org.springframework.data.relational.core.mapping.Column
import java.util.*


data class Person(@Id @Column("id") val personId: UUID?, val age: Int) : Persistable<UUID> {

    @JsonIgnore
    override fun getId(): UUID? = personId

    @JsonIgnore
    override fun isNew(): Boolean = true
}

