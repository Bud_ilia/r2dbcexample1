package com.study.r2dbcexample1.repository

import com.study.r2dbcexample1.model.Person
import com.study.r2dbcexample1.model.id.PersonId
import org.springframework.data.repository.reactive.ReactiveCrudRepository


interface PersonRepository : ReactiveCrudRepository<Person, PersonId>