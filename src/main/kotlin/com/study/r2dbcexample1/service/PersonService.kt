package com.study.r2dbcexample1.service

import com.study.r2dbcexample1.dto.PersonDTO
import com.study.r2dbcexample1.model.Person
import com.study.r2dbcexample1.repository.PersonRepository
import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactive.awaitFirst
import org.springframework.stereotype.Service
import java.util.*

@Service
class PersonService(private val repo: PersonRepository) {
    suspend fun save(dto: PersonDTO) = Person(dto.id?.value ?: UUID.randomUUID(), dto.age)
        .apply { repo.save(this).awaitFirst() }

    fun getAll() = repo.findAll().asFlow()
}