package com.study.r2dbcexample1.dto

import com.study.r2dbcexample1.model.id.PersonId

class PersonDTO(val id: PersonId?, val age: Int)