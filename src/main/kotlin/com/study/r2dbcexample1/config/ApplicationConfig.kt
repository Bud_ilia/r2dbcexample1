package com.study.r2dbcexample1.config

import org.springframework.context.annotation.Configuration
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories
import org.springframework.web.reactive.config.EnableWebFlux

@Configuration
@EnableR2dbcRepositories
@EnableWebFlux
class ApplicationConfig