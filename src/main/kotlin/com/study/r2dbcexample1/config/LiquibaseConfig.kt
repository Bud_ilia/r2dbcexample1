package com.study.r2dbcexample1.config

import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties
import org.springframework.boot.autoconfigure.liquibase.LiquibaseDataSource
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.jdbc.datasource.DriverManagerDataSource
import javax.sql.DataSource


@Configuration
class LiquibaseConfig (private val liquibaseProperties: LiquibaseProperties) {

    @Bean
    @LiquibaseDataSource
    fun liquibaseDataSource(): DataSource =
        DataSourceBuilder.create()
            .type(DriverManagerDataSource::class.java)
            .url(liquibaseProperties.url)
            .username(liquibaseProperties.user)
            .password(liquibaseProperties.password)
            .build()
}