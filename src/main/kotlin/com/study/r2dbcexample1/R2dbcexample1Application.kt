package com.study.r2dbcexample1

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class R2dbcexample1Application

fun main(args: Array<String>) {
    runApplication<R2dbcexample1Application>(*args)
}
