package com.study.r2dbcexample1.controller

import com.study.r2dbcexample1.dto.PersonDTO
import com.study.r2dbcexample1.service.PersonService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/person/")
class PersonController(private val service: PersonService) {

    @PostMapping
    suspend fun save(@RequestBody person: PersonDTO) = service.save(person)

    @GetMapping("/get")
    fun getAll() = service.getAll()
}